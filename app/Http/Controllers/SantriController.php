<?php

namespace App\Http\Controllers;

use App\Models\santri;
use Illuminate\Http\Request;

class SantriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $santris = santri::all();
            
            return view('santri.index',compact('santris'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $model = new santri;
        return view('santri.create',compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new santri;
        $model->nama =$request->nama;
        $model->tgl =$request->tgl;
        $model->alamat =$request->alamat;
        $model->jenis_kelamin =$request->jenis_kelamin;
        $model->daerah =$request->daerah;
        $model->no_bilik =$request->no_bilik;
        $model->save();

        return redirect('santri');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = santri::find($id);
        return view('santri.edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = santri::find($id);
        $model->nama =$request->nama;
        $model->tgl =$request->tgl;
        $model->alamat =$request->alamat;
        $model->jenis_kelamin =$request->jenis_kelamin;
        $model->daerah =$request->daerah;
        $model->no_bilik =$request->no_bilik;
        $model->save();

        return redirect('santri');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = santri::find($id);
        $model->delete();
        return redirect('santri');
    }
}
