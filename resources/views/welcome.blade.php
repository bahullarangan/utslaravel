<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PP.MUB</title>
    <style type="text/css">
        *{
            margin: 0px;
            padding: 0px;
            font-family: sans-serif;
        }
        header{
            background-image: linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url(../views/image/santri.JPG);
            height: 100vh;
            background-size: cover;
            background-position: center;
        }
        ul{
            float: right;
            list-style-type: none;
            margin-top: 25px;
            margin-right: 30px;
        }
        ul li{
            display: inline-block;
        }
        ul li a{
            text-decoration: none;
            color: #fff;
            padding: 5px 20px;
            border: 1px solid;
            transition: 0.6s;
            margin-top: 10px;
           
        }
        ul li a:hover{
            background-color: #fff;
            color: #000;
        }
        ul li.active a{
            background-color: #fff;
            color: #000;
        }
        .logo h1{
            float: left;
            margin-top: 25px;
            font-family:open; 
            color: green;
            border: 1px solid;
            margin-left: 15px;
        }
        marquee{
            color: aqua;
            margin-top: 15px;
            
        }
        
    </style>
</head>
<body>
    
    <header >
    <marquee behavior="" direction="">SELAMAT DATANG DI PP.MIFTAHUL ULUM BETTE PAMEKASAN </marquee>
   
        <div class="main">
            <div class="logo">
               
            <marquee behavior="" direction="up"> <h1>PP.MUB</h1></marquee>
            </div>
           
            <ul>
                <li class="active"><a href="{{url('welcome')}}">Home</a></li>
                <li><a href="{{url('santri')}}">Data</a></li>
                <li><a href="">Abaout</a></li>
                <li><a href="">Service</a></li>
                <li><a href="">Contac</a></li>
            </ul>
        </div>
    </header>
    
</body>
</html>